//bowtie2
nextflow.enable.dsl = 2
params.outdir = "./out/"
params.fastaIn = "./testdata/MK625182.fasta"
params.fastqIn = "./testdata/SRR1777174_2.fastq"

process bowtie2 {
    publishDir "${params.outdir}", mode : "copy", overwrite: true
    container "https://depot.galaxyproject.org/singularity/bowtie2%3A2.4.4--py39hbb4e92a_0"

    input:
        path fastaIn
        path fastqIn
    output:
        path "${fastqIn.getSimpleName()}.sam"

    script:
    """
    bowtie2-build ${fastaIn} ./test
    #bowtie2-build ${fastaIn} . > bowtieBuild
    bowtie2 -x test -U ${fastqIn} -p ${task.cpus} -S ${fastqIn.getSimpleName()}.sam
    """

}

process samSort {

    container "https://depot.galaxyproject.org/singularity/samtools%3A1.9--h91753b0_8" 
    script:
    """
    samtools sort -o ../TmpOut/testSamSort.sam -O sam \
      ../ngsGit/ngsmod/bowtie2/out/SRR1777174_2.sam
    """

}

process bcftools {
    container "https://depot.galaxyproject.org/singularity/bcftools%3A1.10.2--h4f4756c_3"

    script:
    """
    bcftools mpileup -Ou -f ../ngsGit/ngsmod/bowtie2/testdata/MK625182.fasta \
      ../TmpOut/testSamSort.sam  | bcftools call -mv -Ov --ploidy 1 -o \
      ../TmpOut/bcfCallTestoutput.vcf
    """
}

/*
workflow bowtie {
    take:
    outdir 
    fastqIn 
    fastaIn

    main:
    bowtie2()
}
*/

workflow {
    fastaInChannel = channel.fromPath(params.fastaIn)
    fastqInChannel = channel.fromPath(params.fastqIn)
    bowtie2(fastaInChannel, fastqInChannel)
}